import React from 'react'

const Node = ({ className, children }) => (
    <div className={`schema-node ${className}`}>
        {children}
    </div>
)

Node.propTypes = {
    className: React.PropTypes.string.isRequired,
    children: React.PropTypes.oneOfType([
        React.PropTypes.arrayOf(React.PropTypes.node),
        React.PropTypes.node
    ])
}

export default Node
