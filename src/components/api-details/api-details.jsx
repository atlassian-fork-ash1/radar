import './api-details.less'

import React from 'react'
import Markdown from 'react-markdown'

const ApiDetails = ({ info }) => {
    const description = info.description ? <div className="description">
        <Markdown source={info.description} skipHtml />
    </div> : null

    const links = []

    if (info.contact) {
        if (info.contact.url) {
            links.push(<li key="contact-url" className="contact-url">
                <a href={info.contact.url}>{info.contact.name || info.contact.url}</a>
            </li>)
        } else if (info.contact.name) {
            links.push(<li key="contact-name" className="contact-name">{info.contact.name}</li>)
        }

        if (info.contact.email) {
            links.push(<li key="contact-email" className="contact-email">
                <a href={`mailto:${info.contact.email}`}>Contact developer</a>
            </li>)
        }
    }

    if (info.termsOfService) {
        links.push(<li key="details-tos" className="details-tos">
            <a href={info.termsOfService}>Terms of Service</a>
        </li>)
    }

    let license = null
    if (info.license) {
        if (info.license.url) {
            license = (<div className="license">
                <a href={info.license.url}>{info.license.name || info.license.url}</a>
            </div>)
        } else if (info.license.name) {
            license = <div className="license">{info.license.name}</div>
        }
    }

    return (<div className="api-details">
            {description}
        <div>
            <ul>
                {links}
            </ul>
        </div>
        <span className="version">{info.version}</span>
            {license}
    </div>)
}

ApiDetails.propTypes = {
    info: React.PropTypes.shape({
        title: React.PropTypes.string.isRequired,
        description: React.PropTypes.string,
        termsOfService: React.PropTypes.string,
        contact: React.PropTypes.shape({
            name: React.PropTypes.string,
            url: React.PropTypes.string,
            email: React.PropTypes.string
        }),
        license: React.PropTypes.shape({
            name: React.PropTypes.string.isRequired,
            url: React.PropTypes.string
        }),
        version: React.PropTypes.string
    }).isRequired
}

export default ApiDetails
