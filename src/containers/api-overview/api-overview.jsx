import './api-overview.less'

import React from 'react'
import { connect } from 'react-redux'

import { linkWithBase } from '../../utils/basepath-util'

import ApiDetails from '../../components/api-details/api-details.jsx'
import ExternalDocs from '../../components/external-docs/external-docs.jsx'
import Page from '../../components/aui/page/page.jsx'
import ImageLink from '../../components/image-link/image-link.jsx'

const Overview = ({ data }) => {
    const imageLinks = [<ImageLink
      key="resource-image-link"
      to="/resource/"
      image={linkWithBase('/images/explore.svg')}
      title="Browse"
      description="Explore the API for information on methods, endpoints"
    />]

    if (data.tags) {
        imageLinks.push(<
        ImageLink
          key="tag-image-link"
          to="/search?q=tag:"
          image={linkWithBase('/images/tags.svg')}
          title="Tags"
          description="Exploring by tag helps you hone in on the endpoints relevant to you"
        />)
    }

    if (data.securityDefinitions) {
        imageLinks.push(<ImageLink
          key="security-image-link"
          to="/security"
          image={linkWithBase('/images/security.svg')}
          title="Security"
          description="Find out what Authentication and Authorization methods this API supports"
        />)
    }

    if (data['x-radar-pages']) {
        for (const page of data['x-radar-pages']) {
            const path = `/meta/${page.key}`
            imageLinks.push(<ImageLink
              key={page.key}
              to={path}
              image={page.icon}
              title={page.title}
              description={page.description}
            />)
        }
    }

    return (<Page>
        <div className="image-links-centered">{imageLinks}</div>
        <div className="api-overview-sections">
            {ExternalDocs.fromOptionalObject(data.externalDocs)}
            <ApiDetails info={data.info} />
        </div>
    </Page>)
}

Overview.propTypes = {
    data: React.PropTypes.object
}

export default connect(s => s, {})(Overview)
