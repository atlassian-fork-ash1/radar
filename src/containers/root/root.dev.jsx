import React from 'react'
import App from '../app.jsx'
import DevTools from './dev-tools.jsx'

const Root = ({ children, location }) => (<div>
    <App children={children} location={location} />
    <DevTools />
</div>)

Root.propTypes = {
    children: React.PropTypes.object,
    location: React.PropTypes.object
}

export default Root
