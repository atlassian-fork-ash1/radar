import React from 'react'

const NodeTitle = ({ title }) => {
    if (!title) {
        return null
    }
    return <span className="node-title">{title}</span>
}

NodeTitle.propTypes = {
    title: React.PropTypes.string
}

export default NodeTitle
