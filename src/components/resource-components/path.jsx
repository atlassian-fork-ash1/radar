import React from 'react'

const Path = ({ value }) => (<div>
    <h3>Resource URL</h3>
    <p>{value}</p>
</div>)

Path.propTypes = {
    value: React.PropTypes.string
}

export default Path
