import './image-link.less'

import React from 'react'
import { Link } from 'react-router'

const ImageLink = ({ title, description, image, to }) => (
    <div className="image-link">
        <Link to={to}>
            <img src={image} alt="link" />
            <h3>{title}</h3>
            <p>{description}</p>
        </Link>
    </div>)

ImageLink.propTypes = {
    title: React.PropTypes.string.isRequired,
    description: React.PropTypes.string.isRequired,
    image: React.PropTypes.string.isRequired,
    to: React.PropTypes.string.isRequired
}

export default ImageLink
