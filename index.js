const _ = require('lodash')
const consolidate = require('consolidate')

/**
 * Renders the provided content into the RADAR template.
 *
 * @param {String} swaggerContent - an OpenAPI 2.0 spec JSON string
 * @param {String} context - the context path; used by RADAR to prefix all constructed URLs
 * @param {object} [extraOptions] - optionally provide extra options to the template
 * @returns {Promise}
 */
exports.render = (swaggerContent, context, extraOptions) =>
    consolidate.mustache(`${__dirname}/templates/index.mustache`, _.defaults({
        data: swaggerContent,
        context
    }, extraOptions))

/**
 * Renders the 404 Not Found template.
 */
exports.notFound = () => consolidate.mustache(`${__dirname}/templates/404.mustache`, {})

/**
 * These are paths your RADAR server will need to make statically available.
 */
exports.STATIC_PATHS = {
    js: `${__dirname}/dist/js`,
    images: `${__dirname}/images`
}
