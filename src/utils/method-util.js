import _ from 'lodash'

export function getSupportedMethods(resource) {
    const supportedMethods = ['get', 'post', 'put', 'patch', 'delete', 'head', 'options']
    const possibleMethods = Object.keys(resource)
    return _.intersection(possibleMethods, supportedMethods).sort((a, b) =>
        supportedMethods.indexOf(a) > supportedMethods.indexOf(b)
    )
}

export function getLozengeClass(method) {
    let lozengeClass
    switch (method) {
    case 'get':
        lozengeClass = 'aui-lozenge-success'
        break
    case 'delete':
        lozengeClass = 'aui-lozenge-error'
        break
    case 'put':
        lozengeClass = 'aui-lozenge-moved'
        break
    case 'post':
        lozengeClass = 'aui-lozenge-new'
        break
    default:
        lozengeClass = ''
    }
    return lozengeClass
}
