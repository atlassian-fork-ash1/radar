import './json-schema.less'

import _ from 'lodash'
import React from 'react'

import ExternalDocs from '../../external-docs/external-docs.jsx'
import { optionalFormatSpecifier } from '../format-specifier/format-specifier.jsx'
import Node from './components/node.jsx'
import NodeDescription from './components/node-description.jsx'
import NodeTitle from './components/node-title.jsx'
import NodeRange from './components/node-range.jsx'
import NodeRequired from './components/node-required.jsx'

const MAX_CYCLIC_NESTING = 2

/* eslint-disable no-use-before-define */
function mapItems(hideReadOnly) {
    return val => {
        if (val.type) {
            return TYPE[val.type](val)
        }

        return Object.keys(val)
            .map(key => {
                if (OF[key]) {
                    return OF[key](val, hideReadOnly)
                } else if (TYPE[key]) {
                    return TYPE[key](val, hideReadOnly)
                }
                return null
            })
    }
}

function renderAllOf(value, hideReadOnly) {
    if (!value.allOf) {
        return null
    }
    const items = value.allOf.map(mapItems(hideReadOnly))
    return (<Node key={`all-of-node-${_.uniqueId()}`} className="all-of-node">
        <NodeTitle title="all of" />
        {items}
    </Node>)
}

function renderAnyOf(value, hideReadOnly) {
    if (!value.anyOf) {
        return null
    }
    const items = value.anyOf.map(mapItems(hideReadOnly))
    return (<Node key={`any-of-node-${_.uniqueId()}`} className="any-of-node">
        <NodeTitle title="any of" />
        {items}
    </Node>)
}

function renderOneOf(value, hideReadOnly) {
    if (!value.oneOf) {
        return null
    }
    const items = value.oneOf.map(mapItems(hideReadOnly))
    return (<Node key={`one-of-node-${_.uniqueId()}`} className="one-of-node">
        <NodeTitle title="one of" />
        {items}
    </Node>)
}

function renderArray(value, hideReadOnly, title, required) {
    return (<Node key={`array-node-${_.uniqueId()}`} className="array-node">
        <NodeTitle title={title || value.title} />
        <span className="node-tag"></span>
        <NodeRange min={value.minItems || 0} max={value.maxItems || '∞'} />
        <NodeRequired required={required} />
        <NodeDescription value={value.description} />
        {TYPE[value.items.type](value.items, hideReadOnly)}
        {renderAnyOf(value.items)}
        {renderAllOf(value.items)}
        {renderOneOf(value.items)}
    </Node>)
}

function renderBoolean(value, hideReadOnly, title, required) {
    return (<Node key={`bool-node-${_.uniqueId()}`} className="boolean-node">
        <NodeTitle title={title || value.title} />
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required} />
        <NodeDescription value={value.description} />
    </Node>)
}

function renderInteger(value, hideReadOnly, title, required) {
    return (<Node key={`integer-node-${_.uniqueId()}`} className="integer-node">
        <NodeTitle title={title || value.title} />
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required} />
        <NodeRange min={value.minimum} max={value.maximum} />
        {value.enum ? renderEnum(value.enum) : null}
        <NodeDescription value={value.description} />
    </Node>)
}

function renderNumber(value, hideReadOnly, title, required) {
    return (<Node key={`number-node-${_.uniqueId()}`} className="number-node">
        <NodeTitle title={title || value.title} />
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required} />
        {value.enum ? renderEnum(value.enum) : null}
        <NodeDescription value={value.description} />
    </Node>)
}

function renderNull(value, hideReadOnly, title, required) {
    return (<Node key={`null-node-${value}`} className="null-node">
        <NodeTitle title={title || value.title} />
        <span className="node-type">{value.type}</span>
        <NodeRequired required={required} />
        <NodeDescription value={value.description} />
    </Node>)
}

function renderObject(value, hideReadOnly, title, required) {
    let isShallow = false
    const properties = []
    if (value.properties) {
        Object.keys(value.properties).forEach(key => {
            const val = value.properties[key]
            if (seenObjects.indexOf(val) !== -1) {
                ++currentNesting
                isShallow = currentNesting >= MAX_CYCLIC_NESTING
            } else if (val.type === 'object' || val.type === 'array') {
                seenObjects.push(val)
            }
        })
        if (!isShallow) {
            Object.keys(value.properties).forEach(key => {
                const prop = value.properties[key]
                const propRequired = value.required && (value.required.indexOf(key) !== -1)
                if (prop && (!hideReadOnly || !prop.readOnly)) {
                    properties.push(findRenderFunction(prop)(prop, hideReadOnly, key, propRequired))
                }
            })
        }
    }

    return (<Node key={`object-node-${_.uniqueId()}`} className="object-node">
        <NodeTitle title={title || value.title} />
        <span className="node-tag"></span>
        <NodeRequired required={required} />
        <NodeDescription value={value.description} />
        {isShallow ? <span>...</span> : properties}
    </Node>)
}

function renderString(value, hideReadOnly, title, required) {
    return (<Node key={`string-node-${_.uniqueId()}`} className="string-node">
        <NodeTitle title={title || value.title} />
        <span className="node-type">
            <span className="type-value">{value.type}</span>
            {optionalFormatSpecifier(value.format)}
        </span>
        <NodeRequired required={required} />
        {value.enum ? renderEnum(value.enum) : null}
        <NodeDescription value={value.description} />
    </Node>)
}

function renderEnum(value) {
    return (<Node key={`enum-node-${_.uniqueId()}`} className="enum-node">
        {(value || []).map(val => <span key={val} className="node-enum">{val}</span>)}
    </Node>)
}

const TYPE = {
    array: renderArray,
    boolean: renderBoolean,
    integer: renderInteger,
    number: renderNumber,
    null: renderNull,
    object: renderObject,
    string: renderString,
    undefined: _.noop
}

const OF = {
    allOf: renderAllOf,
    anyOf: renderAnyOf,
    oneOf: renderOneOf,
    undefined: _.noop
}

function findRenderFunction(value) {
    let fn = null
    Object.keys(TYPE).forEach(key => {
        if (value.type === key) {
            fn = TYPE[key]
        }
    })
    Object.keys(OF).forEach(key => {
        if (value[key]) {
            fn = OF[key]
        }
    })
    return fn
}

let currentNesting
let seenObjects = []
export default class JsonSchema extends React.Component {
    render() {
        const { data, hideReadOnly } = this.props
        const renderFn = findRenderFunction(data)
        currentNesting = 0
        seenObjects = []
        if (!renderFn) {
            return null
        }
        return (<div className="json-schema">{renderFn(data, hideReadOnly)}
            {ExternalDocs.fromOptionalObject(data.externalDocs)}
        </div>)
    }
}

JsonSchema.propTypes = {
    data: React.PropTypes.object,
    hideReadOnly: React.PropTypes.bool
}
