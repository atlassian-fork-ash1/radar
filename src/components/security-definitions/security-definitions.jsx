import './security-definitions.less'

import React from 'react'
import Markdown from 'react-markdown'

import ApiKey from './types/apiKey.jsx'
import OAuth from './types/oauth.jsx'

const definitionTypes = {
    API_KEY: 'apiKey',
    BASIC: 'basic',
    OAUTH2: 'oauth2'
}

export default class SecurityDefinitions extends React.Component {

    static definition(key, value) {
        const description = value.description ?
            <Markdown source={value.description} skipHtml /> : null
        let supplementary = null
        if (value.type === definitionTypes.API_KEY) {
            supplementary = <ApiKey loc={value.in} name={value.name} />
        } else if (value.type === definitionTypes.OAUTH2) {
            supplementary = (<OAuth
              flow={value.flow}
              authorizationUrl={value.authorizationUrl}
              tokenUrl={value.tokenUrl}
              scopes={value.scopes}
            />)
        }

        return (<div key={`${value.type}-definition`} className="security-definition">
            <h4>{SecurityDefinitions.toName(value.type, key)}</h4>
            {description}
            {supplementary}
        </div>)
    }

    static toName(type, key) {
        switch (type) {
        case 'basic':
            return 'Basic'
        case 'apiKey':
            return 'API Key'
        case 'oauth2':
            return 'OAuth2'
        default:
            return key
        }
    }

    render() {
        const { definitions } = this.props
        const definitionComponents = []

        Object.keys(definitions).forEach(key => {
            definitionComponents.push(SecurityDefinitions.definition(key, definitions[key]))
        })

        const description = 'This API supports the following ways to authenticate requests:'

        return (<div className="security-definitions">
            <span className="security-description">{description}</span>
            {definitionComponents}
        </div>)
    }
}

SecurityDefinitions.propTypes = {
    definitions: React.PropTypes.object
}
