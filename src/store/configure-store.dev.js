import { createStore, compose } from 'redux'

import DevTools from '../containers/root/dev-tools.jsx'

const createStoreWithMiddleware = compose(
    DevTools.instrument()
)(createStore)

export default function configureStore(reducers) {
    return createStoreWithMiddleware(reducers)
}
