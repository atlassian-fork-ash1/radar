import './breadcrumbs.less'

import React from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'

import { splitPath } from '../../reducers/data-reducer'
import { qualifyResourcePath, unqualifyResourcePath } from '../../utils/route-util'

const Breadcrumbs = ({ sections }) => (<ol className="aui-nav aui-nav-breadcrumbs">
    {sections.map((value, index) => {
        if (index === 0) {
            return null
        }
        const last = index + 1 === sections.length
        if (last) {
            return <li className="selected-crumb" key={value.path}>{value.name}</li>
        }
        return (<li key={value.path}>
            <Link to={qualifyResourcePath(value.path)}>{value.name}</Link>
        </li>)
    })}
</ol>)

Breadcrumbs.propTypes = {
    sections: React.PropTypes.arrayOf(React.PropTypes.shape({
        name: React.PropTypes.string.isRequired,
        path: React.PropTypes.string.isRequired
    })).isRequired
}

export default connect(() => ({
    sections: splitPath(unqualifyResourcePath(location.pathname))
}), {})(Breadcrumbs)
