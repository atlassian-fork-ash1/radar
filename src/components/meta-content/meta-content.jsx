import './meta-content.less'

import React from 'react'
import Markdown from 'react-markdown'

const MetaContent = ({ page }) => <Markdown source={page.content} />

MetaContent.propTypes = {
    page: React.PropTypes.shape({
        content: React.PropTypes.string.isRequired
    })
}

export default MetaContent
