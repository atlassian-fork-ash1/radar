import React from 'react'

const CodeList = ({ title, values }) => {
    if (values) {
        return (<div className="resource-section">
            <h4>{title}</h4>
            <ul>
                {values.map(value => <li key={value}>
                    <pre>{value}</pre>
                </li>)}
            </ul>
        </div>)
    }
    return null
}

CodeList.propTypes = {
    title: React.PropTypes.string,
    values: React.PropTypes.arrayOf(React.PropTypes.string)
}

export default CodeList
