import './search-results.less'

import _ from 'lodash'
import React from 'react'
import { connect } from 'react-redux'

import { filterPaths, filterPathsByTag, filterPathsByScope } from '../../reducers/data-reducer'
import { linkWithBase } from '../../utils/basepath-util'

import Page from '../../components/aui/page/page.jsx'
import PathList from '../../components/path-list/path-list.jsx'
import TagList from '../../components/tag-list/tag-list.jsx'
import ScopeList from '../../components/scope-list/scope-list.jsx'

const SearchResults = ({ data, location }) => {
    const { q } = location.query
    let results
    if (q.indexOf('tag:') === 0) {
        if (q === 'tag:') {
            return <Page><TagList tags={data.tags} /></Page>
        }
        results = filterPathsByTag(data.paths, q.replace('tag:', ''))
    } else if (q.indexOf('scope:') === 0) {
        if (q === 'scope:') {
            return (<Page><ScopeList
              scopes={_.find(data.securityDefinitions, { type: 'oauth2' }).scopes}
            /></Page>)
        }
        results = filterPathsByScope(data.paths, q.replace('scope:', ''))
    } else {
        results = filterPaths(data.paths, q)
    }

    if (Object.keys(results).length === 0) {
        return (<Page><div className="empty-search">
            <img src={linkWithBase('/images/hunter.png')} alt="Hunter" />
            <h3>We couldn't find any results matching '{q}'.</h3>
            <p>Hunter, our highly trained search dog, seems to have lost the scent.</p>
        </div></Page>)
    }
    return <Page><PathList resources={results} /></Page>
}

SearchResults.propTypes = {
    data: React.PropTypes.object,
    location: React.PropTypes.object
}

export default connect(state => state, {})(SearchResults)
