import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getResourcesForPath } from '../reducers/data-reducer'

import Breadcrumbs from '../components/breadcrumbs/breadcrumbs.jsx'
import Page from '../components/aui/page/page.jsx'
import PathList from '../components/path-list/path-list.jsx'
import Index from '../components/index/index.jsx'
import Resource from '../components/resource/resource.jsx'

import { unqualifyResourcePath } from '../utils/route-util'

class RestResource extends Component {

    content(unqualifiedPath) {
        const { data } = this.props
        const contextSection = getResourcesForPath(unqualifiedPath, data.paths)
        let content
        if (contextSection.resource) {
            content = (<Resource
              basePath={data.basePath}
              resource={contextSection.resource}
              path={unqualifiedPath}
              securityDefinitions={data.securityDefinitions}
            />)
        } else {
            content = <PathList resources={contextSection.subResources} />
        }

        return (<div>
            <Breadcrumbs />
            <div className="resource-list">
                {content}
            </div>
        </div>)
    }

    render() {
        const { data, location } = this.props
        const unqualifiedPath = unqualifyResourcePath(location.pathname)
        return (<Page
          nav={<Index paths={data.paths} path={unqualifiedPath} />}
        >
            {this.content(unqualifiedPath)}
        </Page>)
    }
}

RestResource.propTypes = {
    data: React.PropTypes.object.isRequired,
    location: React.PropTypes.object.isRequired
}

export default connect(s => s, {})(RestResource)
