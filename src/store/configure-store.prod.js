import { createStore } from 'redux'

export default function configureStore(reducers) {
    return createStore(reducers)
}
