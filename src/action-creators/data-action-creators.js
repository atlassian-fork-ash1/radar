export const DATA_LOAD = 'DATA_LOAD'

export function dataLoad(data) {
    return { type: DATA_LOAD, data }
}
