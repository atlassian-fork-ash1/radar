import React from 'react'

const NodeRange = ({ min, max }) => {
    if (!min && !max) {
        return null
    }
    return <span className="node-range">{`${(min || 0)}..${(max || '∞')}`}</span>
}

NodeRange.propTypes = {
    max: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string]),
    min: React.PropTypes.number
}

export default NodeRange
